#pragma once

#define _USE_MATH_DEFINES
#include "LiborMarketModel.h"
#include "CapletKernel.cuh"
#include <algorithm>
#include <math.h>
#include <iostream>
#include <string>
#include <omp.h>

class Caplet
{
public:
	Caplet(int numRates, int numFactors, int numPaths, double DeltaT, double Kstrike, NormalSim* normalSimu, double *initialRates);
	~Caplet();
	void comparisonPrices(bool print);
	void deltasBlackFormula(bool print, bool onlyLast);
	void deltasFiniteDifference(double h, bool print, bool onlyLast);
	void deltasPathwiseDerivativeForward(bool print, bool onlyLast);
	void deltasPathwiseDerivativeForwardOptimized(bool print, bool onlyLast);
	void deltasPathwiseDerivativeAdjoint(bool print, bool onlyLast);
	void deltasPathwiseDerivativeAdjointOptimized(bool print, bool onlyLast);
	void deltasPathwiseDerivativeAdjointOptimizedOpenMP(bool print, bool onlyLast);
	void deltasPathwiseDerivativeAdjointOptimizedGPU(bool print, bool onlyLast);
	double *deltasBlack;
	double *deltasFD;
	double *deltasPDForward;
	double *deltasPDForwardOptimized;
	double *deltasPDAdjoint;
	double *deltasPDAdjointOptimized;
	double *deltasPDAdjointOptimizedOpenMP;
	double *deltasPDAdjointOptimizedGPU;
	void printDeltas(double* deltas, bool onlyLast);
	void printDeltasInCsv(string folderPath, string name, double *deltas, float time, int nRates, bool onlyLast);
	void printPricesInCsv(string folderPath, string name);
private:
	int nRates;
	int nFactors;
	int nPaths;
	double deltaT;
	double K;
	double *initRates;
	NormalSim* normalSim;
	LiborMarketModel *libor;
	void pricesBlackFormula(double* meanDiscountedPayoffs);
	void discountedPayoffs(double* discountedPayoffs, bool onlyLast);
	void meanDiscountedPayoffs(double* meanDiscountedPayoffs, bool onlyLast);
	void printPrices(double* prices);
	void matrixD(int kPath, double *D);
	void printMatrixD(double *D);
	void matrixDplane(int kPath, double *Dplane, int planNum);
	void derivativesDiscountedPayoffs(int kPath, double *V, double *rates, bool onlyLast);
	void printMatrixV(double *V);
	void propagateMatrixDeltaForward(int kPath, double *V, double *deltaMat, double *deltaMatPrev, double *deltaMatIdentity, double *Dplane, bool onlyLast);
	void printDeltaMat(double *deltaMat);
	void propagateMatrixDeltaForwardOptimized(int kPath, double *V, double *deltaMat, double *deltaMatPrev, double *deltaMatIdentity, bool onlyLast);

};

