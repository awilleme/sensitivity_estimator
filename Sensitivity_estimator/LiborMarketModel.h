#pragma once
#include "NormalSim.h"
#include <iostream>
#include <math.h> 
#include <time.h>
#include <sstream>
#include <fstream>
#include <direct.h>
#include <iostream>
using namespace std;

class LiborMarketModel
{
public:
	LiborMarketModel(int numPaths, int numFactors, int numRates, double DeltaT);
	~LiborMarketModel();
	void setInitialRate(int index, double rate);
	void setAllInitialRates(double *initialRates);
	void simulate(int kPath, NormalSim *normalSim);
	void simulateWithRef(int kPath, NormalSim *normalSim, double *ratesPrivate, double *SPrivate);
	void printRates();
	void printRatesInCsv(string folderPath, string name, NormalSim *normalSim);
	double *rates;
	double* squaredCov;

private:
	int nPaths;
	int nRates;
	int nFactors;
	double *sigma;
	double deltaT;
	double *S;
	double *mu;
	void computeCovarianceMatrix();
	void computeSquaredCovarianceMatrix();
	void setAllInitialRatesWithRef(double *ratesPrivate);
};
