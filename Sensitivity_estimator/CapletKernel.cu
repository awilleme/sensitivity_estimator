#include "CapletKernel.cuh"

__device__ void device_simulateLMM_1factor(int nRates, double* normalSim, double* rates)
{
	double initialRate = 0.035;
	for (int i = 0; i < nRates; i++) {
		rates[i*nRates] = initialRate;
	}
	double S;
	double tmpRate;
	double tmp, tmp2;
	double mu;
	double idxTmp;
	for (int n = 0; n < nRates - 1; n++)
	{
		//loop on rates
		for (int i = 0; i < nRates; i++) {
			if (i < n + 1)
			{
				S = 0.0;
			}
			else
			{
				//compute the sum of equation 8 of smoking adjoints
				S = 0.0;
				for (int j = n + 1; j < i + 1; j++)
				{
					tmpRate = rates[j * nRates + n];
					//tmp = (sigma[j] * deltaT * tmpRate / (1.0 + (deltaT * tmpRate)));
					tmp = (0.2 * 0.5 * tmpRate / (1.0 + (0.5 * tmpRate)));
					S += tmp;
				}
			}
			//mu = S*sigma[i];
			mu = S * 0.2;
			//compute eq 7 of smoking adjoints
			//tmp = (mu - 0.5*sigma[i]*sigma[i]) * deltaT;
			tmp = (mu - 0.5*0.2*0.2) * 0.5;
			//tmp2 = sigma[i] * normalSim[n];
			tmp2 = 0.2 * normalSim[n];
			//tmp2 = tmp2 * sqrt(deltaT);
			tmp2 = tmp2 * sqrt(0.5);
			if (i > n) {
				rates[i*nRates + (n + 1)] = rates[i*nRates + n] * exp(tmp + tmp2);
			}
			else {
				rates[i * nRates + (n + 1)] = 0.0;
			}
		}
	}
}

__device__ void device_simulateLMM_1factorFloat(int nRates, float* normalSim, float* rates)
{
	float initialRate = 0.035;
	for (int i = 0; i < nRates; i++) {
		rates[i*nRates] = initialRate;
	}
	float S;
	float tmpRate;
	float tmp, tmp2;
	float mu;
	float idxTmp;
	for (int n = 0; n < nRates - 1; n++)
	{
		//loop on rates
		for (int i = 0; i < nRates; i++) {
			if (i < n + 1)
			{
				S = 0.0;
			}
			else
			{
				//compute the sum of equation 8 of smoking adjoints
				S = 0.0;
				for (int j = n + 1; j < i + 1; j++)
				{
					tmpRate = rates[j * nRates + n];
					//tmp = (sigma[j] * deltaT * tmpRate / (1.0 + (deltaT * tmpRate)));
					tmp = (0.2 * 0.5 * tmpRate / (1.0 + (0.5 * tmpRate)));
					S += tmp;
				}
			}
			//mu = S*sigma[i];
			mu = S * 0.2;
			//compute eq 7 of smoking adjoints
			//tmp = (mu - 0.5*sigma[i]*sigma[i]) * deltaT;
			tmp = (mu - 0.5*0.2*0.2) * 0.5;
			//tmp2 = sigma[i] * normalSim[n];
			tmp2 = 0.2 * normalSim[n];
			//tmp2 = tmp2 * sqrt(deltaT);
			tmp2 = tmp2 * sqrt(0.5);
			if (i > n) {
				rates[i*nRates + (n + 1)] = rates[i*nRates + n] * exp(tmp + tmp2);
			}
			else {
				rates[i * nRates + (n + 1)] = 0.0;
			}
		}
	}
}


__device__ void device_derivativesDiscountedPayoffs(int nRates, double* V, double* rates, bool onlyLast)
{
	double deltaT = 0.5;
	double K = 0.03;
	double discount = 1.0;
	double rate, payoff, indexFunc;
	//loop over numerator of derivative
	for (int i = 0; i < nRates; i++) {
		rate = rates[i*nRates + i];
		discount *= 1 / (1 + rate * deltaT);
		//if onlyLast, we just compute that for the last one
		if (!onlyLast || i == nRates - 1) {
			payoff = (rate - K);
			payoff = (payoff > 0.0) ? payoff : 0.0;
			indexFunc = (payoff > 0.0) ? 1.0 : 0.0;
			//loop over denominator of derivative --> must check indexes
			for (int j = 0; j < i + 1; j++) {
				//cfr equa 5.4 thesis
				if (i == j) {
					V[i*nRates + j] = deltaT * discount * (indexFunc - (payoff * deltaT / (1 + rates[j*nRates + j] * deltaT)));
				}
				else {
					V[i*nRates + j] = deltaT * discount * payoff * (-deltaT) / (1.0 + rates[j*nRates + j] * deltaT);
				}
			}
		}
	}
}


__device__ void device_derivativesDiscountedPayoffsFloat(int nRates, float* V, float* rates, bool onlyLast)
{
	float deltaT = 0.5;
	float K = 0.03;
	float discount = 1.0;
	float rate, payoff, indexFunc;
	//loop over numerator of derivative
	for (int i = 0; i < nRates; i++) {
		rate = rates[i*nRates + i];
		discount *= 1 / (1 + rate * deltaT);
		//if onlyLast, we just compute that for the last one
		if (!onlyLast || i == nRates - 1) {
			payoff = (rate - K);
			payoff = (payoff > 0.0) ? payoff : 0.0;
			indexFunc = (payoff > 0.0) ? 1.0 : 0.0;
			//loop over denominator of derivative --> must check indexes
			for (int j = 0; j < i + 1; j++) {
				//cfr equa 5.4 thesis
				if (i == j) {
					V[i*nRates + j] = deltaT * discount * (indexFunc - (payoff * deltaT / (1 + rates[j*nRates + j] * deltaT)));
				}
				else {
					V[i*nRates + j] = deltaT * discount * payoff * (-deltaT) / (1.0 + rates[j*nRates + j] * deltaT);
				}
			}
		}
	}
}


__global__ void kernel_deltasAdjointOnCUDA_GlobalMemory(bool onlyLast, int nRates, int nFactors, int nPaths, double *normalSim, double *mainArray)
{
	int numThread = blockIdx.x*blockDim.x + threadIdx.x;
	double futureRate;
	double currentRate;
	double *tmp;
	double tmpData;
	double *Vprev = &(mainArray[numThread*nRates*nRates]);
	double *V = &(mainArray[nPaths*nRates*nRates+numThread*nRates*nRates]);
	double *tmpRates = &(mainArray[2*nPaths*nRates*nRates + numThread * nRates*nRates]);
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//valid for nFactor=1
	device_simulateLMM_1factor(nRates, &(normalSim[numThread*nRates*nFactors]), tmpRates);
	device_derivativesDiscountedPayoffs(nRates, Vprev, tmpRates, onlyLast);
	for (int n = nRates - 2; n >= 0; n--) {
		//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
		for (int i = iStartIndex; i < nRates; i++) {
			for (int j = 0; j < nRates; j++) {
				if (j <= n) {
					V[i*nRates + j] = Vprev[i*nRates + j];
				}
				else {
					futureRate = tmpRates[j*nRates + (n + 1)];
					currentRate = tmpRates[j*nRates + n];
					tmpData = 0.0;
					for (int o = j; o < nRates; o++) {
						//tmpData += libor->squaredCov[j*nRates + o] * Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
						tmpData += 0.2*0.2* Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
					}
					V[i*nRates + j] = futureRate * Vprev[i*nRates + j] / currentRate;
					//V[i*nRates + j] += tmpData * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
					V[i*nRates + j] += tmpData * 0.5*0.5 / ((1 + 0.5 * currentRate)*(1 + 0.5 * currentRate));
				}
			}
		}
		//change pointers
		tmp = V;
		V = Vprev;
		Vprev = tmp;
	}
}

__global__ void kernel_deltasAdjointOnCUDA_LocalMemoryDynamic(bool onlyLast, int nRates, int nFactors, int nPaths, double *normalSim, double *deltas)
{
	int numThread = blockIdx.x*blockDim.x + threadIdx.x;
	double futureRate;
	double currentRate;
	double *tmp;
	double tmpData;
	double *Vprev = new double[nRates*nRates];
	double *V = new double[nRates*nRates];
	double *tmpRates = new double[nRates*nRates];
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//valid for nFactor=1
	device_simulateLMM_1factor(nRates, &(normalSim[numThread*nRates*nFactors]), tmpRates);
	device_derivativesDiscountedPayoffs(nRates, Vprev, tmpRates, onlyLast);
	for (int n = nRates - 2; n >= 0; n--) {
		//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
		for (int i = iStartIndex; i < nRates; i++) {
			for (int j = 0; j < nRates; j++) {
				if (j <= n) {
					V[i*nRates + j] = Vprev[i*nRates + j];
				}
				else {
					futureRate = tmpRates[j*nRates + (n + 1)];
					currentRate = tmpRates[j*nRates + n];
					tmpData = 0.0;
					for (int o = j; o < nRates; o++) {
						//tmpData += libor->squaredCov[j*nRates + o] * Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
						tmpData += 0.2*0.2* Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
					}
					V[i*nRates + j] = futureRate * Vprev[i*nRates + j] / currentRate;
					//V[i*nRates + j] += tmpData * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
					V[i*nRates + j] += tmpData * 0.5*0.5 / ((1 + 0.5 * currentRate)*(1 + 0.5 * currentRate));
				}
			}
		}
		//change pointers
		tmp = V;
		V = Vprev;
		Vprev = tmp;
	}
	for (int i = 0; i < nRates; i++) {
		for (int j = 0; j < nRates; j++) {
			deltas[numThread*nRates*nRates+i*nRates + j] = Vprev[i*nRates + j];
		}
	}
	delete[] Vprev;
	delete[] V;
	delete[] tmpRates;
}

__global__ void kernel_deltasAdjointOnCUDA_LocalMemoryStatic(bool onlyLast, int nRates, int nFactors, int nPaths, double *normalSim, double *deltas)
{
	int numThread = blockIdx.x*blockDim.x + threadIdx.x;
	double futureRate;
	double currentRate;
	double *tmp;
	double tmpData;
	double VprevArray[NRATESFIXED*NRATESFIXED];
	double *Vprev;
	Vprev = VprevArray;
	double VArray[NRATESFIXED*NRATESFIXED];
	double *V;
	V = VArray;
	double tmpRates[NRATESFIXED*NRATESFIXED];
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//valid for nFactor=1
	device_simulateLMM_1factor(nRates, &(normalSim[numThread*nRates*nFactors]), tmpRates);
	device_derivativesDiscountedPayoffs(nRates, Vprev, tmpRates, onlyLast);
	for (int n = nRates - 2; n >= 0; n--) {
		//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
		for (int i = iStartIndex; i < nRates; i++) {
			for (int j = 0; j < nRates; j++) {
				if (j <= n) {
					V[i*nRates + j] = Vprev[i*nRates + j];
				}
				else {
					futureRate = tmpRates[j*nRates + (n + 1)];
					currentRate = tmpRates[j*nRates + n];
					tmpData = 0.0;
					for (int o = j; o < nRates; o++) {
						//tmpData += libor->squaredCov[j*nRates + o] * Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
						tmpData += 0.2*0.2* Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
					}
					V[i*nRates + j] = futureRate * Vprev[i*nRates + j] / currentRate;
					//V[i*nRates + j] += tmpData * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
					V[i*nRates + j] += tmpData * 0.5*0.5 / ((1 + 0.5 * currentRate)*(1 + 0.5 * currentRate));
				}
			}
		}
		//change pointers
		tmp = V;
		V = Vprev;
		Vprev = tmp;
	}
	for (int i = 0; i < nRates; i++) {
		for (int j = 0; j < nRates; j++) {
			//Only works on double for computation capab > 6.x
			//atomicAdd((double*) &(deltas[i * nRates + j]), (double) Vprev[i*nRates + j]);
			deltas[numThread*nRates*nRates + i * nRates + j] = Vprev[i*nRates + j];
		}
	}
}

__global__ void kernel_deltasAdjointOnCUDA_LocalMemoryStaticFloat(bool onlyLast, int nRates, int nFactors, int nPaths, float *normalSim, float *deltas)
{
	int numThread = blockIdx.x*blockDim.x + threadIdx.x;
	float futureRate;
	float currentRate;
	float *tmp;
	float tmpData;
	float VprevArray[NRATESFIXED*NRATESFIXED];
	float *Vprev;
	Vprev = VprevArray;
	float VArray[NRATESFIXED*NRATESFIXED];
	float *V;
	V = VArray;
	float tmpRates[NRATESFIXED*NRATESFIXED];
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//valid for nFactor=1
	device_simulateLMM_1factorFloat(nRates, &(normalSim[numThread*nRates*nFactors]), tmpRates);
	device_derivativesDiscountedPayoffsFloat(nRates, Vprev, tmpRates, onlyLast);
	for (int n = nRates - 2; n >= 0; n--) {
		//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
		for (int i = iStartIndex; i < nRates; i++) {
			for (int j = 0; j < nRates; j++) {
				if (j <= n) {
					V[i*nRates + j] = Vprev[i*nRates + j];
				}
				else {
					futureRate = tmpRates[j*nRates + (n + 1)];
					currentRate = tmpRates[j*nRates + n];
					tmpData = 0.0;
					for (int o = j; o < nRates; o++) {
						//tmpData += libor->squaredCov[j*nRates + o] * Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
						tmpData += 0.2*0.2* Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
					}
					V[i*nRates + j] = futureRate * Vprev[i*nRates + j] / currentRate;
					//V[i*nRates + j] += tmpData * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
					V[i*nRates + j] += tmpData * 0.5*0.5 / ((1 + 0.5 * currentRate)*(1 + 0.5 * currentRate));
				}
			}
		}
		//change pointers
		tmp = V;
		V = Vprev;
		Vprev = tmp;
	}
	for (int i = 0; i < nRates; i++) {
		for (int j = 0; j < nRates; j++) {
			deltas[numThread*nRates*nRates + i * nRates + j] = Vprev[i*nRates + j];
		}
	}
}


//example cfr https://devblogs.nvidia.com/using-shared-memory-cuda-cc/
__global__ void kernel_deltasAdjointOnCUDA_SharedMemory(bool onlyLast, int nRates, int nFactors, int nPaths, double *normalSim, double *deltas, int BLOCK_SIZE)
{
	extern __shared__ double mainArray[];
	int numThread = threadIdx.x;
	double futureRate;
	double currentRate;
	double *tmp;
	double tmpData;
	double *Vprev = &(mainArray[numThread*nRates*nRates]);
	double *V = &(mainArray[BLOCK_SIZE*nRates*nRates + numThread * nRates*nRates]);
	double *tmpRates = &(mainArray[2 * BLOCK_SIZE*nRates*nRates + numThread * nRates*nRates]);
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//valid for nFactor=1
	device_simulateLMM_1factor(nRates, &(normalSim[numThread*nRates*nFactors]), tmpRates);
	device_derivativesDiscountedPayoffs(nRates, Vprev, tmpRates, onlyLast);
	for (int n = nRates - 2; n >= 0; n--) {
		//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
		for (int i = iStartIndex; i < nRates; i++) {
			for (int j = 0; j < nRates; j++) {
				if (j <= n) {
					V[i*nRates + j] = Vprev[i*nRates + j];
				}
				else {
					futureRate = tmpRates[j*nRates + (n + 1)];
					currentRate = tmpRates[j*nRates + n];
					tmpData = 0.0;
					for (int o = j; o < nRates; o++) {
						//tmpData += libor->squaredCov[j*nRates + o] * Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
						tmpData += 0.2*0.2* Vprev[i*nRates + o] * tmpRates[o*nRates + n + 1];
					}
					V[i*nRates + j] = futureRate * Vprev[i*nRates + j] / currentRate;
					//V[i*nRates + j] += tmpData * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
					V[i*nRates + j] += tmpData * 0.5*0.5 / ((1 + 0.5 * currentRate)*(1 + 0.5 * currentRate));
				}
			}
		}
		//change pointers
		tmp = V;
		V = Vprev;
		Vprev = tmp;
	}
}

//cfr. https://www.quantstart.com/articles/Monte-Carlo-Simulations-In-CUDA-Barrier-Option-Pricing
//cfr. https://stackoverflow.com/questions/49769638/no-kernel-image-is-available-for-execution-on-the-device
void deltasAdjointInCUDA(bool onlyLast, int nRates, int nFactors, int nPaths, NormalSim *normalSim,  double *deltas) {
	unsigned BLOCK_SIZE = 1024;
	unsigned GRID_SIZE = ceil(float(nPaths) / float(BLOCK_SIZE));
	double *deltasTmp = new double[nPaths*nRates*nRates];
	double *mainArray;
	//mainArray is used for Vprev, V and rates!
	CUDA_CALL(cudaMalloc((void**)&mainArray, 3*nPaths*nRates*nRates*sizeof(double)));
	double *deltasGPU;
	// ---- Use of Global memory of GPU
	//that is tricky, depending on wheter nRates is odd or even, we should use first or second part of mainArray
	//deltasGPU = (nRates % 2) ? &(mainArray[0]) : &(mainArray[nPaths*nRates*nRates]);
	//kernel_deltasAdjointOnCUDA_GlobalMemory<<<GRID_SIZE, BLOCK_SIZE>>>(onlyLast, nRates, nFactors, nPaths, normalSim->simulationsGPU, mainArray);
	// ---- Use of Static Local memory of threads GPU
	deltasGPU = mainArray;
	kernel_deltasAdjointOnCUDA_LocalMemoryStatic << <GRID_SIZE, BLOCK_SIZE >> > (onlyLast, nRates, nFactors, nPaths, normalSim->simulationsGPU, mainArray);
	// ---- Use of Dynamic Local memory of threads GPU
	//deltasGPU = mainArray;
	//kernel_deltasAdjointOnCUDA_LocalMemoryDynamic <<<GRID_SIZE, BLOCK_SIZE >>>(onlyLast, nRates, nFactors, nPaths, normalSim->simulationsGPU, mainArray);
	// ---- Use of Local memory of threads GPU
	//BLOCK_SIZE = 8;
	//GRID_SIZE = ceil(float(nPaths) / float(BLOCK_SIZE));
	//deltasGPU = mainArray;
	//shoud not exceed 48KB : https://stackoverflow.com/questions/36176133/cuda-dynamic-shared-memory-triggers-thrustsystemsystem-error
	//const unsigned sharedMemorySize = BLOCK_SIZE*sizeof(double)*3*nRates*nRates;
	//cout << "Shared mem size: " << sharedMemorySize << "\n";
	//kernel_deltasAdjointOnCUDA_SharedMemory<<<GRID_SIZE,BLOCK_SIZE,sharedMemorySize>>>(onlyLast, nRates, nFactors, nPaths, normalSim->simulationsGPU, mainArray, BLOCK_SIZE);

	// Wait for GPU to finish before accessing on host
	cudaDeviceSynchronize();
	// Check for any errors launching the kernel
	cudaError_t cudaStatus;
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		printf("KERNEL launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}
	//recover and aggregate deltas
	CUDA_CALL(cudaMemcpy(deltasTmp, deltasGPU, nPaths*nRates*nRates * sizeof(double), cudaMemcpyDeviceToHost));
	for (int k = 0; k < nPaths; k++) {
		for (int i = 0; i < nRates; i++) {
			for (int j = 0; j < nRates; j++) {
				deltas[i*nRates + j] += deltasTmp[k*nRates*nRates+i*nRates + j];
			}
		}
	}
	//compute mean
	for (int i = 0; i < nRates; i++) {
		for (int j = 0; j < nRates; j++) {
			deltas[i*nRates + j] /= nPaths;
		}
	}
	CUDA_CALL(cudaFree(mainArray));
	delete[] deltasTmp;
}

//cfr. https://www.quantstart.com/articles/Monte-Carlo-Simulations-In-CUDA-Barrier-Option-Pricing
//cfr. https://stackoverflow.com/questions/49769638/no-kernel-image-is-available-for-execution-on-the-device
void deltasAdjointInCUDAFloat(bool onlyLast, int nRates, int nFactors, int nPaths, NormalSim *normalSim, double *deltas) {
	unsigned BLOCK_SIZE = 1024;
	unsigned GRID_SIZE = ceil(float(nPaths) / float(BLOCK_SIZE));
	float *deltasTmp = new float[nPaths*nRates*nRates];
	float *mainArray;
	//mainArray is used for Vprev, V and rates!
	CUDA_CALL(cudaMalloc((void**)&mainArray, nPaths*nRates*nRates * sizeof(float)));
	float *deltasGPU;
	// ---- Use of Static Local memory of threads GPU
	deltasGPU = mainArray;
	kernel_deltasAdjointOnCUDA_LocalMemoryStaticFloat << <GRID_SIZE, BLOCK_SIZE >> > (onlyLast, nRates, nFactors, nPaths, normalSim->simulationsGPUFloat, mainArray);

	// Wait for GPU to finish before accessing on host
	cudaDeviceSynchronize();
	// Check for any errors launching the kernel
	cudaError_t cudaStatus;
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		printf("KERNEL launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}
	//recover and aggregate deltas
	CUDA_CALL(cudaMemcpy(deltasTmp, deltasGPU, nPaths*nRates*nRates * sizeof(float), cudaMemcpyDeviceToHost));
	for (int k = 0; k < nPaths; k++) {
		for (int i = 0; i < nRates; i++) {
			for (int j = 0; j < nRates; j++) {
				deltas[i*nRates + j] += deltasTmp[k*nRates*nRates + i * nRates + j];
			}
		}
	}
	//compute mean
	for (int i = 0; i < nRates; i++) {
		for (int j = 0; j < nRates; j++) {
			deltas[i*nRates + j] /= nPaths;
		}
	}
	CUDA_CALL(cudaFree(mainArray));
	delete[] deltasTmp;
}