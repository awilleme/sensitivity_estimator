#pragma once
#include <iostream>
#include <string>
#include <random>
#include <chrono>
#include <stdio.h>
#include <cuda_runtime.h>

using namespace std;
class NormalSim
{
public:
	NormalSim(int nSimWidth, int nSimHeight, int nSimDepth);
	~NormalSim();
	void simulate();
	double *simulations; 
	float *simulationsFloat;
	double *simulationsGPU;
	float *simulationsGPUFloat;
private:
	//for CPU
	std::default_random_engine *generator;
	std::normal_distribution<double> *distribution;
	int width;
	int height;
	int depth;
};

