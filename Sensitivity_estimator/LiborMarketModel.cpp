﻿#include "LiborMarketModel.h"


LiborMarketModel::LiborMarketModel(int numPaths, int numFactors, int numRates, double DeltaT)
{
	nPaths = numPaths;
	nRates = numRates;
	nFactors = numFactors;
	rates = new double[nRates*nRates];
	sigma = new double[nRates*nFactors];
	squaredCov = new double[nRates*nRates];
	S = new double[nFactors];
	mu = new double[nRates];
	computeCovarianceMatrix();
	computeSquaredCovarianceMatrix();
	deltaT = DeltaT; 
}


LiborMarketModel::~LiborMarketModel()
{
	delete[] rates;
	delete[] sigma;
	delete[] squaredCov;
	delete[] S;
	delete[] mu;
}


//set all rates for time = 0
void LiborMarketModel::setAllInitialRates(double *initRates)
{
	//init rates
	int i;
	int idx = 0;
	for (i = 0; i < nRates; i++)
	{
		rates[i*nRates]= initRates[i];
	}
}

//set all rates for time = 0
void LiborMarketModel::setAllInitialRatesWithRef(double *ratesPrivate)
{
	//init rates
	int i;
	int idx = 0;
	for (i = 0; i < nRates; i++)
	{
		ratesPrivate[i*nRates] = rates[i*nRates];
	}
}

//set initial rate "index"
void LiborMarketModel::setInitialRate(int index, double rate)
{
	//init rates
	rates[index*nRates] = rate;
}

//compute the covariance matrix \sigma
//Glasserman 1999: j drawn randomly from the uniform distribution on 0 : 15; 0:25.
void LiborMarketModel::computeCovarianceMatrix()
{
	int idx = 0;
	//FIXME cfr. page 28 (code = page 63) of thesis or page 334 of Joshi 2003 or page 183 of Glasserman
	for (int i = 0; i < nRates; i++)
	{
		for (int j = 0; j < nFactors; j++)
		{
			//to test
			//sigma->m[idx] = 0.1*(1+j) * 2*(i+1);
			sigma[idx] = 0.2;
			idx++;
		}
	}
}

//compute the squared covariance matrix \sigma
void LiborMarketModel::computeSquaredCovarianceMatrix()
{
	for (int i = 0; i < nRates; i++)
	{
		for (int j = 0 ; j < nRates; j++) {
			squaredCov[i*nRates + j] = 0.0;
			for (int n = 0; n < nFactors; n++)
			{
				squaredCov[i*nRates + j] = squaredCov[i*nRates + j] + sigma[i*nFactors+n] * sigma[j*nFactors + n];
			}
		}
	}
}

//Compute paths for LMM
//FIXME: is it possible to avoid looping on zero elements
void LiborMarketModel::simulate(int kPath, NormalSim *normalSim)
{
	int i, j, n, d;
	double tmp, tmp2, tmpRate;

	int idxPathFactor = kPath * nRates * nFactors;
	int idxTmp = 0;

	//loop on times
	for (n = 0; n < nRates - 1; n++)
	{
		//loop on rates
		for (i = 0; i<nRates; i++){
			if (i < n + 1)
			{
				for (d = 0; d < nFactors; d++)
				{
					S[d] = 0.0;
				}
			}
			else
			{
				//compute for each component of S
				for (d = 0; d < nFactors; d++)
				{
					//compute the sum of equation 8 of smoking adjoints
					S[d] = 0.0;
					for (j = n + 1; j < i + 1; j++)
					{
						tmpRate = rates[j * nRates + n];
 						tmp = (sigma[j*nFactors + d] * deltaT * tmpRate / (1.0 + (deltaT * tmpRate)));
						S[d] = tmp + S[d];
					}
				}
			}
			mu[i] = 0.0;
			idxTmp = i * nFactors;
			for (d = 0; d < nFactors; d++)
			{
				mu[i] = mu[i] + (S[d] * sigma[idxTmp]);
				idxTmp++;
			}
			//compute eq 7 of smoking adjoints
			tmp = (mu[i] - 0.5*squaredCov[i*nRates+i]) * deltaT;
			tmp2 = 0.0;
			idxTmp = i * nFactors;
			for (d = 0; d<nFactors; d++){
				tmp2 = tmp2 + sigma[idxTmp + d] * normalSim->simulations[idxPathFactor+d*nRates+n];
				idxTmp++;
			}
			tmp2 = tmp2 * sqrt(deltaT);
			if (i > n){
				rates[i*nRates+(n+1)] = rates[i*nRates + n] * exp(tmp + tmp2);
			}
			else{
				rates[i * nRates + (n + 1)] = 0.0;
			}
		}
	}
}

//store the rates in the attached pointer
void LiborMarketModel::simulateWithRef(int kPath, NormalSim *normalSim, double *ratesPrivate, double *SPrivate)
{
	int i, j, n, d;
	double tmp, tmp2, tmpRate, muPrivate;

	int idxPathFactor = kPath * nRates * nFactors;
	int idxTmp = 0;
	setAllInitialRatesWithRef(ratesPrivate);
	//loop on times
	for (n = 0; n < nRates - 1; n++)
	{
		//loop on rates
		for (i = 0; i < nRates; i++) {
			if (i < n + 1)
			{
				for (d = 0; d < nFactors; d++)
				{
					SPrivate[d] = 0.0;
				}
			}
			else
			{
				//compute for each component of S
				for (d = 0; d < nFactors; d++)
				{
					//compute the sum of equation 8 of smoking adjoints
					SPrivate[d] = 0.0;
					for (j = n + 1; j < i + 1; j++)
					{
						tmpRate = ratesPrivate[j * nRates + n];
						tmp = (sigma[j*nFactors + d] * deltaT * tmpRate / (1.0 + (deltaT * tmpRate)));
						SPrivate[d] = tmp + SPrivate[d];
					}
				}
			}
			muPrivate = 0.0;
			idxTmp = i * nFactors;
			for (d = 0; d < nFactors; d++)
			{
				muPrivate = muPrivate + (SPrivate[d] * sigma[idxTmp]);
				idxTmp++;
			}
			//compute eq 7 of smoking adjoints
			tmp = (muPrivate - 0.5*squaredCov[i*nRates + i]) * deltaT;
			tmp2 = 0.0;
			idxTmp = i * nFactors;
			for (d = 0; d < nFactors; d++) {
				tmp2 = tmp2 + sigma[idxTmp + d] * normalSim->simulations[idxPathFactor + d * nRates + n];
				idxTmp++;
			}
			tmp2 = tmp2 * sqrt(deltaT);
			if (i > n) {
				ratesPrivate[i*nRates + (n + 1)] = ratesPrivate[i*nRates + n] * exp(tmp + tmp2);
			}
			else {
				ratesPrivate[i * nRates + (n + 1)] = 0.0;
			}
		}
	}
}


void LiborMarketModel::printRates()
{
	int i, n;
	for (i = 0; i < nRates; i++){
		for (n = 0; n < nRates; n++){
			printf("%-7f    ", rates[i*nRates + n]);
			//cout << rates[i*nRates + n] << ";";
		}
		cout << "\n";
	}
}

void LiborMarketModel::printRatesInCsv(string folderPath, string name, NormalSim *normalSim) {
	int i, j, k;
	std::ofstream myfile;
	//create directory
	_mkdir((folderPath).c_str());
	string nameWithoutDot(name);
	//replace dots by o's in the file name
	std::replace(nameWithoutDot.begin(), nameWithoutDot.end(), '.', 'o');
	//create csv file
	folderPath = folderPath + "\\" + nameWithoutDot + ".csv";
	myfile.open(folderPath);
	for (k = 0; k < nPaths; k++) {
		simulate(k, normalSim);
		for (i = 0; i < nRates; i++) {
			for (j = 0; j < nRates; j++) {
				if (j != 0)
					myfile << ";";
				myfile << rates[i*nRates + j];
			}
			if(i!=nRates-1)
				myfile << ";";
		}
		myfile << "\n";
	}
	myfile << "\n";
	myfile.close();

}
