#include "Caplet.h"

using namespace std;

Caplet::Caplet(int numRates, int numFactors, int numPaths, double DeltaT, double Kstrike, NormalSim* normalSimu, double *initialRates)
{
	nRates = numRates;
	nFactors = numFactors;
	nPaths = numPaths;
	deltaT = DeltaT;
	K = Kstrike;
	normalSim = normalSimu;
	initRates = new double[numRates];
	std::copy(initialRates, initialRates+nRates, initRates);
	libor = new LiborMarketModel(nPaths, nFactors, nRates, deltaT);
	libor->setAllInitialRates(initRates);
	//the delta of each rates wrt each rate
	deltasBlack = new double[nRates*nRates];
	std::fill_n(deltasBlack, nRates*nRates, 0.0);
	deltasFD = new double[nRates*nRates];
	std::fill_n(deltasFD, nRates*nRates, 0.0);
	deltasPDForward = new double[nRates*nRates];
	std::fill_n(deltasPDForward, nRates*nRates, 0.0);
	deltasPDForwardOptimized = new double[nRates*nRates];
	std::fill_n(deltasPDForwardOptimized, nRates*nRates, 0.0);
	deltasPDAdjoint = new double[nRates*nRates];
	std::fill_n(deltasPDAdjoint, nRates*nRates, 0.0);
	deltasPDAdjointOptimized = new double[nRates*nRates];
	std::fill_n(deltasPDAdjointOptimized, nRates*nRates, 0.0);
	deltasPDAdjointOptimizedOpenMP = new double[nRates*nRates];
	std::fill_n(deltasPDAdjointOptimizedOpenMP, nRates*nRates, 0.0);
	deltasPDAdjointOptimizedGPU = new double[nRates*nRates];
	std::fill_n(deltasPDAdjointOptimizedGPU, nRates*nRates, 0.0);
}


Caplet::~Caplet()
{
	delete libor;
	delete[] initRates;
	delete[] deltasBlack;
	delete[] deltasFD;
	delete[] deltasPDForward;
	delete[] deltasPDForwardOptimized;
	delete[] deltasPDAdjoint;
	delete[] deltasPDAdjointOptimized;
	delete[] deltasPDAdjointOptimizedOpenMP;
	delete[] deltasPDAdjointOptimizedGPU;
}

void Caplet::comparisonPrices(bool print) {
	double* pricesBlack = new double[nRates];
	double* pricesMC = new double[nRates];
	pricesBlackFormula(pricesBlack);
	if (print) {
		cout << "Prices according to Black formula\n";
		printPrices(pricesBlack);
	}
	meanDiscountedPayoffs(pricesMC, false);
	if (print) {
		cout << "Prices computed with Monte Carlo\n";
		printPrices(pricesMC);
	}

	//compute mean and variance of error
	double mean= 0;
	double var = 0;
	int n;
	for (n = 0; n < nRates; n++)
	{
		mean += (pricesMC[n]-pricesBlack[n]);
	}
	mean = mean / nRates;
	for (n = 0; n < nRates; n++)
	{
		var += ((pricesMC[n]-pricesBlack[n]) - mean) * ((pricesMC[n] - pricesBlack[n]) - mean);
	}
	var /= nRates;
	cout << "Mean of error " << mean << "\n";
	cout << "Variance of error " << var << "\n";

	delete[] pricesBlack;
	delete[] pricesMC;
}

//exact price according to Black equation. Cfr. pg 6 of Glasserman 1999
void Caplet::pricesBlackFormula(double* meanDiscountedPayoffs) {
	int i;
	double T = 0.0;
	double sqrtT;
	double r = 0.0;
	//discount factor cfr pg 4 Glasserman 1999
	double b = 1.0;
	double sigma, sigmaSquared;
	double tmp;
	double d1 = 0.0;
	double d2 = 0.0;
	for (i = 0; i < nRates; i++) {
		T = deltaT*i;
		sqrtT = sqrt(T);
		sigmaSquared = libor->squaredCov[i*nRates + i];
		sigma = sqrt(sigmaSquared);
		//we take the initial rates
		r = libor->rates[i*nRates];
		b = b / (1 + deltaT * r);
		tmp = log(r / K);
		d1 = (tmp + 0.5*sigmaSquared*T) / (sqrtT*sigma);
		d2= (tmp - 0.5*sigmaSquared*T) / (sqrtT*sigma);
		//cout << "i " << i << " T " << T << " r " << r << " b " << b << " sigma " << sigma << " tmp " << tmp << " d1 " << d1 << " d2 " << d2 << "\n";
		//cout << "0.5*erfc(-d1*sqrt(0.5)) " << 0.5*erfc(-d1 * sqrt(0.5)) << " 0.5*erfc(-d2*sqrt(0.5)) " << 0.5*erfc(-d2 * sqrt(0.5)) << "\n";
		// use erfc to compute cdf of gaussian https://stackoverflow.com/questions/2328258/cumulative-normal-distribution-function-in-c-c
		meanDiscountedPayoffs[i] = deltaT * b*(r*0.5*erfc(-d1 * sqrt(0.5)) - K * 0.5*erfc(-d2 * sqrt(0.5)));
	}
}

//Exact version - derived from Black formula, deduced from pg 6 of Glasserman 1999
void Caplet::deltasBlackFormula(bool print, bool onlyLast) {
	int i, j;
	double Li, Lj, T, sqrtT, sigmaSquared, sigma, tmp, d1, d2;
	int idx = 0;
	double* pricesBlack = new double[nRates];
	pricesBlackFormula(pricesBlack);
	double b = 1.0;
	double pdfGaussian;
	//loop over numerator of delta
	for (i = 0; i < nRates; i++) {
		Li = libor->rates[i*nRates];
		T = deltaT * i;
		sqrtT = sqrt(T);
		sigmaSquared = libor->squaredCov[i*nRates + i];
		sigma = sqrt(sigmaSquared);
		//we take the initial rates
		b = b / (1 + deltaT * Li);
		tmp = log(Li / K);
		d1 = (tmp + 0.5*sigmaSquared*T) / (sqrtT*sigma);
		d2 = (tmp - 0.5*sigmaSquared*T) / (sqrtT*sigma);
		//loop over denominator of delta
		for (j = 0; j < nRates; j++) {
			Lj = libor->rates[j*nRates];
			if (j <= i) {
				deltasBlack[idx] = -pricesBlack[i]*deltaT/(1+deltaT*Lj);
				if (j == i) {
					deltasBlack[idx] += deltaT*b*0.5*erfc(-d1 * sqrt(0.5));
					//to avoid the first step
					if (T != 0.0) {
						//cfr definition of pdf of a gaussian
						pdfGaussian = (1 / (sqrt(2 * M_PI))) * exp(-0.5 * pow(d1, 2.0));
						deltasBlack[idx] += deltaT * b / sigma / sqrtT * pdfGaussian;
						//if d(log(x/a)) = 1/(xa) -> not correct
						//deltasBlack[idx] += deltaT * b / K / sigma / sqrtT * pdfGaussian;
						pdfGaussian = (1 / (sqrt(2 * M_PI))) * exp(-0.5 * pow(d2, 2.0));
						deltasBlack[idx] -= deltaT * b * K /Li / sigma / sqrtT * pdfGaussian;
						//ok if d(log(x/a)) = 1/(xa) -> not correct
						//deltasBlack[idx] -= deltaT * b / Li / sigma / sqrtT * pdfGaussian;
					}
				}
			}
			else {
				deltasBlack[idx] = 0.0;
			}
			idx++;
		}
	}
	if (print)
		printDeltas(deltasBlack, onlyLast);
	delete[] pricesBlack;
}


void Caplet::discountedPayoffs(double* discountedPayoffs, bool onlyLast) {
	int i, k;
	int idx = 0;
	int idx2 = 0;
	double rate;
	double payoff;
	for (k = 0; k < nPaths; k++) {
		libor->simulate(k, normalSim);
		double discount = 1.0;
		idx2 = 0;
		for (i = 0; i < nRates; i++) {
			rate = libor->rates[idx2];
			discount *= 1 / (1 + rate * deltaT);
			if (!onlyLast || (i == nRates - 1)) {
				payoff = (rate - K);
				payoff = (payoff > 0.0) ? deltaT * payoff : 0.0;
				discountedPayoffs[idx] = discount * payoff;
				//cout << "rate " << rate << "\n";
				//cout << "payoff " << payoff << "\n";
				//cout << "discount " << discount << "\n";
				//cout << "discountedPayoffs[idx] " << discountedPayoffs[idx] << "\n";
			}
			else {
				discountedPayoffs[idx] = 0.0;
			}
			idx++;
			idx2 += (nRates + 1);
		}
	}
}

void Caplet::meanDiscountedPayoffs(double* meanDiscountedPayoffs, bool onlyLast) {
	int i, k;
	int idx = 0;
	double rate;
	double payoff;
	for (k = 0; k < nPaths; k++) {
		libor->simulate(k, normalSim);
		double discount = 1.0;
		idx = 0;
		for (i = 0; i < nRates; i++) {
			if (k == 0)
				meanDiscountedPayoffs[i] = 0.0;
			rate = libor->rates[idx];
			discount *= 1 / (1 + rate * deltaT);
			if (!onlyLast || (i == nRates - 1)) {
				payoff = (rate - K);
				payoff = (payoff > 0.0) ? deltaT * payoff : 0.0;
				meanDiscountedPayoffs[i] += discount * payoff;
				//cout << "rate " << rate << "\n";
				//cout << "payoff " << payoff << "\n";
				//cout << "discount " << discount << "\n";
				//cout << "meanDiscountedPayoffs[i] " << meanDiscountedPayoffs[i] << "\n";
			}
			idx += (nRates + 1);
		}
	}
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	for (i = iStartIndex; i < nRates; i++) {
		meanDiscountedPayoffs[i] = meanDiscountedPayoffs[i] / nPaths;
	}
}


void Caplet::printPrices(double* prices) {
	int i;
	cout << "Prices :\n";
	for (i = 0; i < nRates; i++) {
		cout << "\tprices[" << i << "] " << prices[i] << "\n";
	}
}

void Caplet::printDeltas(double* deltas, bool onlyLast) {
	int i, j;
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	cout << "Delta of caplet on rate i (on rows) with respect to rate j (on cols):\n";
	for (i = iStartIndex; i < nRates; i++) {
		cout << "\t" << i << "  ";
		for (j = 0; j < nRates; j++) {
			printf("%-7f    ", deltas[i*nRates+j]);
		}
		cout << "\n";
	}
}


void Caplet::deltasFiniteDifference(double h, bool print, bool onlyLast)
{
	int i,j;
	double* tmpInitRates = new double[nRates];
	double* pricesNext = new double[nRates];
	double* pricesPrev = new double[nRates];
	double scale = 1 / (2 * h);
	std::copy(initRates, initRates + nRates, tmpInitRates);
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//print progress line
	if (print) {
		for (i = 0; i < nRates; i++) {
			cout << "-";
		}
		cout << ">\n";
	}
	//loop over denominator of delta
	for (j = 0; j < nRates; j++) {
		//compute the price of caplet for value L_j^0 (theta+h);
		tmpInitRates[j] += h;
		libor->setInitialRate(j, tmpInitRates[j]);
		meanDiscountedPayoffs(pricesNext, onlyLast);
		//printPrices(pricesNext);

		//compute the price of caplet for value L_j^0 (theta-h);
		tmpInitRates[j] -= 2*h;
		libor->setInitialRate(j, tmpInitRates[j]);
		meanDiscountedPayoffs(pricesPrev, onlyLast);
		//printPrices(pricesPrev);

		//compute the delta with finite difference
		//loop over numerator of delta
		for (i = iStartIndex; i < nRates; i++) {
			deltasFD[i*nRates+j] = (pricesNext[i] - pricesPrev[i])*scale;
		}
		//come back to initial value for rate j
		tmpInitRates[j] += h;
		libor->setInitialRate(j, tmpInitRates[j]);
		if (print)
			cout << "x";
	}
	if (print) {
		cout << "\n";
		printDeltas(deltasFD, onlyLast);
	}
	delete[] tmpInitRates;
	delete[] pricesNext;
	delete[] pricesPrev;
}

//complexity is <O(m^2) as we do not double loop
void Caplet::matrixD(int kPath, double *D){
	int n,i,j;
	int idxDiag = 0;
	int offsetDiag = nRates+1;
	double *tmpRates = libor->rates;
	//set matrix D to zeros
	std::fill_n(D, nRates*nRates*(nRates - 1), 0);
	double futureRate, currentRate;
	//h=deltaT here
	double squaredStep = deltaT * deltaT;
	for (n = 0; n < nRates - 1; n++) {
		//compute elements on diagonal of D
		for (i = 0; i < nRates;  i++) {
			if (i <= n) {
				D[idxDiag] = 1.0;
			}
			else {
				futureRate = tmpRates[i*nRates + (n + 1)];
				currentRate = tmpRates[i*nRates + n];
				D[idxDiag] = futureRate / currentRate;
				D[idxDiag] += futureRate * squaredStep * libor->squaredCov[i*nRates+i]/ ((1 + deltaT * futureRate)*(1 + deltaT * futureRate));
			}
			idxDiag += offsetDiag;
		}
		idxDiag -= nRates;
		//compute non-zero elements off diagonal
		for (j = n+1; j < nRates; j++) {
			for (i = j + 1; i < nRates; i++) {
				futureRate = tmpRates[i*nRates + (n + 1)];
				currentRate = tmpRates[j*nRates + n];
				D[n*nRates*nRates + i * nRates + j] = futureRate* libor->squaredCov[i*nRates + j]*squaredStep/((1+deltaT* currentRate)*(1 + deltaT * currentRate));
			}
		}
	}
	//printMatrixD(D);
}

//it only gives one time element of matrix D (one plane)
void Caplet::matrixDplane(int kPath, double *Dplane, int nPlane) {
	int i, j;
	int idxDiag = 0;
	int offsetDiag = nRates + 1;
	double *tmpRates = libor->rates;
	//set matrix D to zeros
	std::fill_n(Dplane, nRates*nRates, 0);
	double futureRate, currentRate;
	//h=deltaT here
	double squaredStep = deltaT * deltaT;
	//compute elements on diagonal of D
	for (i = 0; i < nRates; i++) {
		if (i <= nPlane) {
			Dplane[idxDiag] = 1.0;
		}
		else {
			futureRate = tmpRates[i*nRates + (nPlane + 1)];
			currentRate = tmpRates[i*nRates + nPlane];
			Dplane[idxDiag] = futureRate / currentRate;
			Dplane[idxDiag] += futureRate * squaredStep * libor->squaredCov[i*nRates + i] / ((1 + deltaT * futureRate)*(1 + deltaT * futureRate));
		}
		idxDiag += offsetDiag;
	}
	idxDiag -= nRates;
	//compute non-zero elements off diagonal
	for (j = nPlane + 1; j < nRates; j++) {
		for (i = j + 1; i < nRates; i++) {
			futureRate = tmpRates[i*nRates + (nPlane + 1)];
			currentRate = tmpRates[j*nRates + nPlane];
			Dplane[i * nRates + j] = futureRate * libor->squaredCov[i*nRates + j] * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
		}
	}
}

void Caplet::printMatrixD(double *D) {
	int i, j, n;
	int idx=0;
	cout << "Matrix D :\n";
	for (n = 0; n < nRates - 1; n++) {
		cout << "\tTime " << n << "\n";
		for (i = 0; i < nRates; i++) {
			cout << "\t\t";
			for (j = 0; j < nRates; j++) {
				cout << D[idx] << " ";
				idx++;
			}
			cout << "\n";
		}
	}
}

// TO CHECK between thesis equa  5.4 and Glasserman 1999 page 9 ---> MUST BE RECHECKED!!
//if onlyLast = false, we compute the deriv of all nRates caplets. If onlyLast = true, we compute only the deriv of last caplet (not optimized).
void Caplet::derivativesDiscountedPayoffs(int kPath, double *V, double *rates, bool onlyLast) {
	int i,j;
	double discount = 1.0;
	double rate, payoff, indexFunc;
	std::fill_n(V, nRates*nRates, 0.0);
	//loop over numerator of derivative
	for (i = 0; i < nRates; i++) {
		rate = rates[i*nRates + i];
		discount *= 1 / (1 + rate * deltaT);
		//if onlyLast, we just compute that for the last one
		if (!onlyLast || i==nRates-1) {
			payoff = (rate - K);
			payoff = (payoff > 0.0) ? payoff : 0.0;
			indexFunc = (payoff > 0.0) ? 1.0 : 0.0;
			//loop over denominator of derivative --> must check indexes
			for (j = 0; j < i + 1; j++) {
				//cfr equa 5.4 thesis
				if (i == j) {
					V[i*nRates + j] = deltaT * discount * (indexFunc - (payoff * deltaT / (1 + rates[j*nRates + j] * deltaT)));
				}
				else {
					V[i*nRates + j] = deltaT * discount * payoff * (-deltaT) / (1.0 + rates[j*nRates + j] * deltaT);
				}
			}
		}
	}
	//printMatrixV(V);
}

void Caplet::printMatrixV(double *V) {
	int i, j;
	int idx = 0;
	cout << "Matrix V (derivativesDiscountedPayoffs) :\n";
	for (i = 0; i < nRates; i++) {
		cout << "\t";
		for (j = 0; j < nRates; j++) {
			printf("%-7f    ", V[idx]);
			idx++;
		}
		cout << "\n";
	}
}

void Caplet::propagateMatrixDeltaForward(int kPath, double *V, double *deltaMat, double *deltaMatPrev, double *deltaMatIdentity, double *Dplane, bool onlyLast){
	int i, j, n, o;
	double *tmpDeltaMat;
	for (n = 0; n < nRates - 1; n++) {
		matrixDplane(kPath, Dplane, n);
		tmpDeltaMat = deltaMatPrev;
		//we must use an identity matrix at n=0 (startup)
		if (n == 0) {
			deltaMatPrev = deltaMatIdentity;
		}
		for (i = 0; i < nRates; i++) {
			for (j = 0; j < nRates; j++) {
				//compute each element of deltaMat
				deltaMat[i * nRates + j] = 0.0;
				for (o = 0; o < nRates; o++) {
					deltaMat[i * nRates + j] += Dplane[i * nRates + o] * deltaMatPrev[o * nRates + j];
				}
			}
		}
		//change pointers
		deltaMatPrev = deltaMat;
		deltaMat = tmpDeltaMat;
	}
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	for (i = iStartIndex; i < nRates; i++) {
		for (j = 0; j < nRates; j++) {
			for (o = 0; o < nRates; o++) {
				deltasPDForward[i*nRates + j] += V[i*nRates + o] * deltaMatPrev[o * nRates + j];
			}
		}
	}
}

//FIXME: DEPRECATED AS DELTAMAT in 3D does not exist anymore
void Caplet::printDeltaMat(double *deltaMat) {
	int i, j, n;
	int idx = 0;
	cout << "Matrix deltaMat :\n";
	for (n = 0; n < nRates; n++) {
		cout << "\tTime " << n << "\n";
		for (i = 0; i < nRates; i++) {
			cout << "\t\t";
			for (j = 0; j < nRates; j++) {
				cout << deltaMat[idx] << " ";
				idx++;
			}
			cout << "\n";
		}
	}
}

//note h = \delta (cfr pg 92 of smoking adjoints)
void Caplet::deltasPathwiseDerivativeForward(bool print, bool onlyLast)
{
	int k, i, j;
	double *Dplane = new double[nRates*nRates];
	double *V = new double[nRates*nRates];
	double *deltaMat = new double[nRates*nRates];
	double *deltaMatPrev = new double[nRates*nRates];
	double *deltaMatIdentity = new double[nRates*nRates];
	int idx = 0;
	//set initial condition for matrix deltaMat -> identity for first plan
	std::fill_n(deltaMatIdentity, nRates*nRates, 0.0);
	for (i = 0; i < nRates; i++) {
		deltaMatIdentity[idx] = 1;
		idx += nRates + 1;
	}
	//loop over paths
	std::fill_n(deltasPDForward, nRates*nRates, 0.0);
	for (k = 0; k < nPaths; k++) {
		libor->simulate(k, normalSim);
		derivativesDiscountedPayoffs(k, V, libor->rates, onlyLast);
		propagateMatrixDeltaForward(k, V, deltaMat, deltaMatPrev, deltaMatIdentity, Dplane, onlyLast);
	}
	//average over all paths
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	for (i = iStartIndex; i < nRates; i++) {
		for (j = 0; j < nRates; j++) {
			deltasPDForward[i*nRates + j] = deltasPDForward[i*nRates + j] / nPaths;
		}
	}
	if (print)
		printDeltas(deltasPDForward, onlyLast);
	delete[] Dplane;
	delete[] V;
	delete[] deltaMat;
	delete[] deltaMatPrev;
	delete[] deltaMatIdentity;
}

//cfr. page 9 of Glasserman 1999 and page 90 of Glasserman 2006 (smoking)
void Caplet::propagateMatrixDeltaForwardOptimized(int kPath, double *V, double *deltaMat, double *deltaMatPrev, double *deltaMatIdentity, bool onlyLast) {
	int i, j, n, k, o;
	double futureRate;
	double currentRate;
	double tmp;
	double *tmpRates = libor->rates;
	double squaredStep = deltaT * deltaT;
	double *tmpDeltaMat;
	for (n = 0; n < nRates - 1; n++) {
		tmpDeltaMat = deltaMatPrev;
		//we must use an identity matrix at n=0 (startup)
		if (n == 0) {
			deltaMatPrev = deltaMatIdentity;
		}
		for (i = 0; i < nRates; i++) {
			for (j = 0; j < nRates; j++) {
				if (i <= n) {
					deltaMat[i * nRates + j] = deltaMatPrev[i * nRates + j];
				}
				else {
					futureRate = tmpRates[i*nRates + (n + 1)];
					currentRate = tmpRates[i*nRates + n];
					tmp = 0.0;
					for (k = n + 1; k <= i; k++) {
						tmp += (libor->squaredCov[i*nRates + k] * deltaMatPrev[k * nRates + j]) / ((1 + deltaT * tmpRates[k*nRates + n])*(1 + deltaT * tmpRates[k*nRates + n]));
					}
					deltaMat[i * nRates + j] = futureRate / currentRate * deltaMatPrev[i * nRates + j];
					deltaMat[i * nRates + j] += futureRate * tmp*squaredStep;
				}
			}
		}
		//change pointers
		deltaMatPrev = deltaMat;
		deltaMat = tmpDeltaMat;
	}
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	for (i = iStartIndex; i < nRates; i++) {
		for (j = 0; j < nRates; j++) {
			for (o = 0; o < nRates; o++) {
				deltasPDForwardOptimized[i*nRates + j] += V[i*nRates + o] * deltaMatPrev[o * nRates + j];
			}
		}
	}
}

//Optimized version of forward method // page 9 of Glasserman 1999 and page 90 of Glasserman 2006 (smoking)
void Caplet::deltasPathwiseDerivativeForwardOptimized(bool print, bool onlyLast)
{
	int k, i, j;
	double *V = new double[nRates*nRates];
	double *deltaMat = new double[nRates*nRates];
	double *deltaMatPrev = new double[nRates*nRates];
	double *deltaMatIdentity = new double[nRates*nRates];
	int idx = 0;
	//set initial condition for matrix deltaMat -> identity for first plan
	std::fill_n(deltaMatIdentity, nRates*nRates, 0.0);
	for (i = 0; i < nRates; i++) {
		deltaMatIdentity[idx] = 1;
		idx += nRates + 1;
	}
	//loop over paths
	std::fill_n(deltasPDForwardOptimized, nRates*nRates, 0.0);
	for (k = 0; k < nPaths; k++) {
		libor->simulate(k, normalSim);
		derivativesDiscountedPayoffs(k, V, libor->rates, onlyLast);
		propagateMatrixDeltaForwardOptimized(k, V, deltaMat, deltaMatPrev, deltaMatIdentity, onlyLast);
	}
	//average over all paths
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	for (i = iStartIndex; i < nRates; i++) {
		for (j = 0; j < nRates; j++) {
			deltasPDForwardOptimized[i*nRates + j] = deltasPDForwardOptimized[i*nRates + j] / nPaths;
		}
	}
	if (print)
		printDeltas(deltasPDForwardOptimized, onlyLast);
	delete[] V;
	delete[] deltaMat;
	delete[] deltaMatPrev;
	delete[] deltaMatIdentity;
}

//if onlyLast = false, we compute the deltas of all nRates caplets. If onlyLast = true, we compute only the delta of last caplet (a lot faster).
void Caplet::deltasPathwiseDerivativeAdjoint(bool print, bool onlyLast)
{
	int k, i, j, n, o;
	double *Dplane = new double[nRates*nRates];
	double *Vprev = new double[nRates*nRates];
	double *V = new double[nRates*nRates];
	double *tmp;
	int idx = 0;
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//loop over paths
	std::fill_n(deltasPDAdjoint, nRates*nRates, 0.0);
	for (k = 0; k < nPaths; k++) {
		libor->simulate(k, normalSim);
		derivativesDiscountedPayoffs(k, Vprev, libor->rates, onlyLast);
		for (n = nRates - 2; n >= 0; n--) {
			matrixDplane(k, Dplane, n);
			//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
			for (i = iStartIndex; i < nRates; i++) {
				for (j = 0; j < nRates; j++) {
					//compute each element of deltaMat
					V[i*nRates + j] = 0.0;
					for (o = 0; o < nRates; o++) {
						//this one is not easy as it is not the same for as Smoking page 90 : each line of V is a vector V in Smoking (no inter-relation)
						V[i*nRates + j] += Dplane[o*nRates + j] * Vprev[i*nRates + o];
					}
				}
			}
			//change pointers
			tmp = V;
			V = Vprev;
			Vprev = tmp;
		}
		for (i = iStartIndex; i < nRates; i++) {
			for (j = 0; j < nRates; j++) {
				deltasPDAdjoint[i*nRates + j] += Vprev[i*nRates + j];
			}
		}
	}
	//average over all paths
	for (i = iStartIndex; i < nRates; i++) {
		for (j = 0; j < nRates; j++) {
			deltasPDAdjoint[i*nRates + j] = deltasPDAdjoint[i*nRates + j] / nPaths;
		}
	}
	if (print)
		printDeltas(deltasPDAdjoint, onlyLast);
	delete[] Dplane;
	delete[] V;
	delete[] Vprev;
}

//if onlyLast = false, we compute the deltas of all nRates caplets. If onlyLast = true, we compute only the delta of last caplet (a lot faster).
void Caplet::deltasPathwiseDerivativeAdjointOptimized(bool print, bool onlyLast)
{
	int k, i, j, n, o;
	double futureRate;
	double currentRate;
	double *Vprev = new double[nRates*nRates];
	double *V = new double[nRates*nRates];
	double *tmp;
	double tmpData;
	double *tmpRates = libor->rates;
	double squaredStep = deltaT * deltaT;
	int idx = 0;
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//loop over paths
	std::fill_n(deltasPDAdjointOptimized, nRates*nRates, 0.0);
	for (k = 0; k < nPaths; k++) {
		libor->simulate(k, normalSim);
		derivativesDiscountedPayoffs(k, Vprev, libor->rates, onlyLast);
		for (n = nRates - 2; n >= 0; n--) {
			//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
			for (i = iStartIndex; i < nRates; i++) {
				for (j = 0; j < nRates; j++) {
					if (j <= n) {
						V[i*nRates + j] = Vprev[i*nRates + j];
					}
					else {
						futureRate = tmpRates[j*nRates + (n + 1)];
						currentRate = tmpRates[j*nRates + n];
						tmpData = 0.0;
						for (o = j; o < nRates; o++) {
							tmpData += libor->squaredCov[j*nRates + o] * Vprev[i*nRates + o] * tmpRates[o*nRates + n+1];
						}
						V[i*nRates + j] = futureRate * Vprev[i*nRates + j] / currentRate;
						V[i*nRates + j] += tmpData * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
					}
				}
			}
			//change pointers
			tmp = V;
			V = Vprev;
			Vprev = tmp;
		}
		for (i = iStartIndex; i < nRates; i++) {
			for (j = 0; j < nRates; j++) {
				//deltasPDAdjointOptimized[i*nRates + j] += libor->rates[i*nRates + j];
				deltasPDAdjointOptimized[i*nRates + j] += Vprev[i*nRates + j];
			}
		}
	}
	//average over all paths
	for (i = iStartIndex; i < nRates; i++) {
		for (j = 0; j < nRates; j++) {
			deltasPDAdjointOptimized[i*nRates + j] = deltasPDAdjointOptimized[i*nRates + j] / nPaths;
		}
	}
	if (print)
		printDeltas(deltasPDAdjointOptimized, onlyLast);
	delete[] V;
	delete[] Vprev;
}

//if onlyLast = false, we compute the deltas of all nRates caplets. If onlyLast = true, we compute only the delta of last caplet (a lot faster).
// cfr https://stackoverflow.com/questions/20413995/reducing-on-array-in-openmp
void Caplet::deltasPathwiseDerivativeAdjointOptimizedOpenMP(bool print, bool onlyLast)
{
	int numThreads = omp_get_max_threads();
	//cout << "Threads:" << numThreads << "\n";
	omp_set_num_threads(numThreads);
	double squaredStep = deltaT * deltaT;
	int iStartIndex = (onlyLast ? (nRates - 1) : 0);
	//loop over paths
	std::fill_n(deltasPDAdjointOptimizedOpenMP, nRates*nRates, 0.0);
	double *deltasPrivate = new double[numThreads*nRates*nRates];
	std::fill_n(deltasPrivate, numThreads*nRates*nRates, 0.0);
	#pragma omp parallel for num_threads(numThreads)
	for (int w = 0; w < numThreads; w++) {
		double futureRate;
		double currentRate;
		double tmpData;
		double *tmp;
		double *Vprev = new double[nRates*nRates];
		double *V = new double[nRates*nRates];
		double *ratesPrivate = new double[nRates*nRates];
		double *SPrivate = new double[nFactors];
		for (int k = w*nPaths/numThreads; k < (w+1)*nPaths/numThreads; k++) {
			libor->simulateWithRef(k, normalSim, ratesPrivate, SPrivate);
			derivativesDiscountedPayoffs(k, Vprev, ratesPrivate, onlyLast);
			for (int n = nRates - 2; n >= 0; n--) {
				//!\\IMPORTANT: if you select i=nRates-1 only, the last line (=deltas of only caplet on rate L_n) will be right but not the others and complexity will be reduced
				for (int i = iStartIndex; i < nRates; i++) {
					for (int j = 0; j < nRates; j++) {
						if (j <= n) {
							V[i*nRates + j] = Vprev[i*nRates + j];
						}
						else {
							futureRate = ratesPrivate[j*nRates + (n + 1)];
							currentRate = ratesPrivate[j*nRates + n];
							tmpData = 0.0;
							for (int o = j; o < nRates; o++) {
								tmpData += libor->squaredCov[j*nRates + o] * Vprev[i*nRates + o] * ratesPrivate[o*nRates + n + 1];
							}
							V[i*nRates + j] = futureRate * Vprev[i*nRates + j] / currentRate;
							V[i*nRates + j] += tmpData * squaredStep / ((1 + deltaT * currentRate)*(1 + deltaT * currentRate));
						}
					}
				}
				//change pointers
				tmp = V;
				V = Vprev;
				Vprev = tmp;
			}
			for (int i = iStartIndex; i < nRates; i++) {
				for (int j = 0; j < nRates; j++) {
					deltasPrivate[w*nRates*nRates + i*nRates + j] += Vprev[i*nRates + j];
				}
			}
		}
		delete[] V;
		delete[] Vprev;
		delete[] SPrivate;
		delete[] ratesPrivate;
	}
	//average over all paths
	for (int i = iStartIndex; i < nRates; i++) {
		for (int j = 0; j < nRates; j++) {
			for (int w = 0; w < numThreads; w++) {
				deltasPDAdjointOptimizedOpenMP[i*nRates + j] += deltasPrivate[w*nRates*nRates + i * nRates + j];
			}
			deltasPDAdjointOptimizedOpenMP[i*nRates + j] = deltasPDAdjointOptimizedOpenMP[i*nRates + j] / nPaths;
		}
	}
	if (print)
		printDeltas(deltasPDAdjointOptimizedOpenMP, onlyLast);

	delete[] deltasPrivate;
}


void Caplet::deltasPathwiseDerivativeAdjointOptimizedGPU(bool print, bool onlyLast)
{
	//deltasAdjointInCUDA(onlyLast, nRates, nFactors, nPaths, normalSim, deltasPDAdjointOptimizedGPU);
	deltasAdjointInCUDAFloat(onlyLast, nRates, nFactors, nPaths, normalSim, deltasPDAdjointOptimizedGPU);
	if(print)
		printDeltas(deltasPDAdjointOptimizedGPU, onlyLast);
}


void Caplet::printDeltasInCsv(string folderPath, string name, double *deltas, float time, int nRates, bool onlyLast) {
	int i, j;
	std::ofstream myfile;
	//create directory
	_mkdir((folderPath).c_str());
	string nameWithoutDot(name);
	//replace dots by o's in the file name
	std::replace(nameWithoutDot.begin(), nameWithoutDot.end(), '.', 'o');
	//create csv file
	string csvPath = folderPath + "\\" + nameWithoutDot + ".csv";
	myfile.open(csvPath);
	myfile << name << "\n";
	myfile << "Time;" << time << "\n";
	int iStartIndex = onlyLast ? nRates - 1 : 0;
	for (i = iStartIndex; i < nRates; i++) {
		myfile << i;
		for (j = 0; j < nRates; j++) {
			myfile << ";" << deltas[i*nRates + j];
		}
		myfile << "\n";
	}
	myfile << "\n";
	myfile.close();
}

void Caplet::printPricesInCsv(string folderPath, string name) {
	int i, k;
	std::ofstream myfile;
	//create directory
	_mkdir((folderPath).c_str());
	string nameWithoutDot(name);
	//replace dots by o's in the file name
	std::replace(nameWithoutDot.begin(), nameWithoutDot.end(), '.', 'o');
	//create csv file
	folderPath = folderPath + "\\" + nameWithoutDot + ".csv";
	myfile.open(folderPath);
	double* pricesBlack = new double[nRates];
	double* pricesMC = new double[nRates];
	pricesBlackFormula(pricesBlack);
	meanDiscountedPayoffs(pricesMC, false);
	//print Black prices first
	for(i=0;i<nRates;i++){
		if (i != 0)
			myfile << ";";
		myfile << pricesBlack[i];
	}
	myfile << "\n";
	//print mean MC prices then
	for (i = 0; i < nRates; i++) {
		if (i != 0)
			myfile << ";";
		myfile << pricesMC[i];
	}
	myfile << "\n";
	double *discPayoffs = new double[nPaths*nRates];
	discountedPayoffs(discPayoffs, false);
	//print prices for each path then
	for (k = 0; k < nPaths; k++) {
		for (i = 0; i < nRates; i++) {
			if (i != 0)
				myfile << ";";
			myfile << discPayoffs[k*nRates+i];
		}
		myfile << "\n";
	}
	delete[] discPayoffs;
	myfile.close();
}