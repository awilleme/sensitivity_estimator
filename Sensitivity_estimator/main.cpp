//#define DO_COMPARISON_PRICES
//#define DO_DELTA_BLACK
//#define DO_DELTA_FINITE_DIFFERENCE
//#define DO_DELTA_PATHWISE_DERIVATIVE_FORWARD
//#define DO_DELTA_PATHWISE_DERIVATIVE_FORWARD_OPTIMIZED
//#define DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT
#define DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED
#define DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED_OPENMP
#define DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED_GPU
//#define DO_COMPUTE_TIMES_DELTAS_IN_CSV
//#define DO_PRINT_RATES_IN_CSV
//#define DO_PRINT_PRICES_IN_CSV


#include <iostream>
#include <string>
#include <sstream>
#include <time.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <direct.h>
#include "Caplet.h"

using namespace std;

void computeTimeDeltas(int nRatesTested, int *ratesTested, int nPathsTested, double initialRate, double deltaT, double K, bool onlyLast);

int main(int argc, char *argv[]) 
{
	int nPaths = 10240;
	int nRates = 60;
	int nFactors = 1;
	bool printDeltas = false;
	//should we compute deltas for last caplet only?
	bool onlyLast = true;

	//parameters for computeTimeDeltas()
	int nRatesTested = 7;
	int ratesTested[7] = { 5, 10, 20, 30, 40, 50, 60};

	NormalSim *normalSim;
	normalSim = new NormalSim(nPaths, nFactors, nRates);
	normalSim->simulate();

	double initialRate = 0.035;
	double *initialRates = new double[nRates];
	for (int i = 0; i < nRates; i++){
		initialRates[i] = initialRate;
	}

	//Compute Caplet deltas using finite difference central-difference method
	Caplet *caplet;
	//the time step
	double deltaT = 0.5; //cfr BSwapTest.m Financial Modelling  - Theory, Implementation and Practice with Matlab
	//the strike 
	double K = 0.03;
	caplet = new Caplet(nRates, nFactors, nPaths, deltaT, K, normalSim, initialRates);
	clock_t tstart, tend;
	#ifndef	DO_COMPUTE_TIMES_DELTAS_IN_CSV
		#ifdef DO_COMPARISON_PRICES
			caplet->comparisonPrices(true);
		#endif
		#ifdef DO_DELTA_BLACK
			cout << "Using derivative of Black formula\n";
			caplet->deltasBlackFormula(printDeltas, onlyLast);
			cout << "\n";
		#endif
		#ifdef DO_DELTA_FINITE_DIFFERENCE
			double h = 0.001;
			cout << "Using finite difference method with h=" << h <<"\n";
			tstart = clock();
			//the increment that changes the initial rates
			caplet->deltasFiniteDifference(h, printDeltas, onlyLast);
			tend = clock();
			cout << "time for caplet->deltasFiniteDifference(): " << (float)(tend - tstart) / CLOCKS_PER_SEC << " s\n\n";
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_FORWARD
			cout << "Using pathwise derivative forward method unoptimized\n";
			tstart = clock();
			caplet->deltasPathwiseDerivativeForward(printDeltas, onlyLast);
			tend = clock();
			cout << "time for caplet->deltasPathwiseDerivativeForward: " << (float)(tend - tstart) / CLOCKS_PER_SEC << " s\n\n";
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_FORWARD_OPTIMIZED
			cout << "Using pathwise derivative forward method optimized\n";
			tstart = clock();
			caplet->deltasPathwiseDerivativeForwardOptimized(printDeltas, onlyLast);
			tend = clock();
			cout << "time for caplet->deltasPathwiseDerivativeForwardOptimized: " << (float)(tend - tstart) / CLOCKS_PER_SEC << " s\n\n";
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT
			cout << "Using pathwise derivative adjoint method\n";
			tstart = clock();
			caplet->deltasPathwiseDerivativeAdjoint(printDeltas, onlyLast);
			tend = clock();
			cout << "time for caplet->deltasPathwiseDerivativeAdjoint(): " << (float)(tend - tstart) / CLOCKS_PER_SEC << " s\n\n";
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED
			cout << "Using pathwise derivative adjoint method optimized\n";
			tstart = clock();
			caplet->deltasPathwiseDerivativeAdjointOptimized(printDeltas, onlyLast);
			tend = clock();
			cout << "time for caplet->deltasPathwiseDerivativeAdjointOptimized(): " << (float)(tend - tstart) / CLOCKS_PER_SEC << " s\n\n";
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED_OPENMP
					cout << "Using pathwise derivative adjoint method optimized with OpenMP on multi-core CPU\n";
					tstart = clock();
					caplet->deltasPathwiseDerivativeAdjointOptimizedOpenMP(printDeltas, onlyLast);
					tend = clock();
					cout << "time for caplet->deltasPathwiseDerivativeAdjointOptimizedOpenMP(): " << (float)(tend - tstart) / CLOCKS_PER_SEC << " s\n\n";
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED_GPU
					cout << "Using pathwise derivative adjoint method optimized with CUDA on GPU\n";
					tstart = clock();
					caplet->deltasPathwiseDerivativeAdjointOptimizedGPU(printDeltas, onlyLast);
					tend = clock();
					cout << "time for caplet->deltasPathwiseDerivativeAdjointOptimizedCUDA(): " << (float)(tend - tstart) / CLOCKS_PER_SEC << " s\n\n";
		#endif
	#endif
	#ifdef DO_COMPUTE_TIMES_DELTAS_IN_CSV
		computeTimeDeltas(nRatesTested, ratesTested, nPaths, initialRate, deltaT, K, onlyLast);
	#endif
	#ifdef DO_PRINT_RATES_IN_CSV
		LiborMarketModel *libor;
		libor = new LiborMarketModel(nPaths, nFactors, nRates, deltaT);
		libor->setAllInitialRates(initialRates);
		string folderPath("D:\\Documents\\Actuariat\\tfe\\implementation\\c++\\Sensitivity_estimator\\Results\\rates\\" + std::to_string((int)std::time(nullptr)));
		std::ostringstream csvName;
		csvName << "Rates_deltaT" << deltaT << "_nPaths" << nPaths << "_nRates" << nRates;
		libor->printRatesInCsv(folderPath, csvName.str(), normalSim);
	#endif
	#ifdef DO_PRINT_PRICES_IN_CSV
		string folderPath("D:\\Documents\\Actuariat\\tfe\\implementation\\c++\\Sensitivity_estimator\\Results\\prices\\" + std::to_string((int)std::time(nullptr)));
		std::ostringstream csvName;
		csvName << "Prices_deltaT" << deltaT << "_nPaths" << nPaths << "_nRates" << nRates;
		caplet->printPricesInCsv(folderPath, csvName.str());
	#endif

	delete normalSim;
	delete caplet;

	cout << "FINISHED \n";

	int tmp = 0;
	cin >> tmp;
	return 0;
}

void computeTimeDeltas(int nRatesTested, int *ratesTested, int nPathsTested, double initialRate, double deltaT, double K, bool onlyLast) {
	NormalSim *normalSim;
	Caplet *caplet;
	std::ostringstream csvName;
	string name;
	clock_t tstart, tend;
	string onlyLastString = onlyLast ? "_onlyLast" : "";
	//set timestamp as folder name
	string folderPath("D:\\Documents\\Actuariat\\tfe\\implementation\\c++\\Sensitivity_estimator\\Results\\deltas\\" + std::to_string((int)std::time(nullptr)));
	for (int i = 0; i < nRatesTested; i++) {
		cout << i << "th rate =  " << ratesTested[i] << "\n";
		//create initial rates
		double *initialRates = new double[ratesTested[i]];
		for (int j = 0; j < ratesTested[i]; j++) {
			initialRates[j] = initialRate;
		}
		//initialize and simulate normal random variables
		normalSim = new NormalSim(nPathsTested, 1, ratesTested[i]);
		normalSim->simulate();
		//create new caplet object
		caplet = new Caplet(ratesTested[i], 1, nPathsTested, deltaT, K, normalSim, initialRates);
		#ifdef DO_DELTA_BLACK
			tstart = clock();
			caplet->deltasBlackFormula(false, onlyLast);
			tend = clock();
			csvName << "Deltas_BlackFormula_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath,csvName.str(), caplet->deltasBlack, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		#ifdef DO_DELTA_FINITE_DIFFERENCE
			tstart = clock();
			double h = 0.001;
			caplet->deltasFiniteDifference(h, false, onlyLast);
			tend = clock();
			csvName << "Deltas_FiniteDifferenceH" << h << "_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath, csvName.str(), caplet->deltasFD, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_FORWARD
			tstart = clock();
			caplet->deltasPathwiseDerivativeForward(false, onlyLast);
			tend = clock();
			csvName << "Deltas_PathwiseDerivativeForward_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath, csvName.str(), caplet->deltasPDForward, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_FORWARD_OPTIMIZED
			tstart = clock();
			caplet->deltasPathwiseDerivativeForwardOptimized(false, onlyLast);
			tend = clock();
			csvName << "Deltas_PathwiseDerivativeForwardOptimized_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath, csvName.str(), caplet->deltasPDForwardOptimized, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT
			tstart = clock();
			caplet->deltasPathwiseDerivativeAdjoint(false, onlyLast);
			tend = clock();
			csvName << "Deltas_PathwiseDerivativeAdjoint_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath, csvName.str(), caplet->deltasPDAdjoint, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED
			tstart = clock();
			caplet->deltasPathwiseDerivativeAdjointOptimized(false, onlyLast);
			tend = clock();
			csvName << "Deltas_PathwiseDerivativeAdjointOptimized_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath, csvName.str(), caplet->deltasPDAdjointOptimized, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED_OPENMP
			tstart = clock();
			caplet->deltasPathwiseDerivativeAdjointOptimizedOpenMP(false, onlyLast);
			tend = clock();
			csvName << "Deltas_PathwiseDerivativeAdjointOptimizedOpenMP_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath, csvName.str(), caplet->deltasPDAdjointOptimizedOpenMP, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		#ifdef DO_DELTA_PATHWISE_DERIVATIVE_ADJOINT_OPTIMIZED_GPU
			tstart = clock();
			caplet->deltasPathwiseDerivativeAdjointOptimizedGPU(false, onlyLast);
			tend = clock();
			csvName << "Deltas_PathwiseDerivativeAdjointOptimizedGPU_deltaT" << deltaT << "_K" << K << "_nPaths" << nPathsTested << "_nRates" << ratesTested[i] << onlyLastString;
			caplet->printDeltasInCsv(folderPath, csvName.str(), caplet->deltasPDAdjointOptimizedGPU, (float)(tend - tstart) / CLOCKS_PER_SEC, ratesTested[i], onlyLast);
			csvName.str("");
		#endif
		delete[] initialRates;
		delete normalSim;
		delete caplet;
	}
}
