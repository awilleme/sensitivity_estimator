#pragma once

#define NRATESFIXED 60

#include <math.h>       /* ceil */
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda_runtime.h>
#include <iostream>
#include "NormalSim.h"
#include "LiborMarketModel.h"
#include "Caplet.h"

//GeForce GT 745M	Compute capibility 3.0

//cfr.https://devtalk.nvidia.com/default/topic/486420/kernel-not-doing-anything/
#define CUDA_CALL(x) {cudaError_t cuda_error__ = (x); if (cuda_error__) printf("CUDA error: " #x " returned \"%s\"\n", cudaGetErrorString(cuda_error__));}



void deltasAdjointInCUDA(bool onlyLast, int nRates, int nFactors, int nPaths, NormalSim *normalSim, double *deltas);
void deltasAdjointInCUDAFloat(bool onlyLast, int nRates, int nFactors, int nPaths, NormalSim *normalSim, double *deltas);