#include "NormalSim.h"

NormalSim::NormalSim(int nSimWidth, int nSimHeight, int nSimDepth)
{
	width = nSimWidth;
	height = nSimHeight;
	depth = nSimDepth;
	simulations = new double[nSimWidth*nSimHeight*nSimDepth];
	simulationsFloat = new float[nSimWidth*nSimHeight*nSimDepth];
	//cfr. https://www.quantstart.com/articles/dev_array_A_Useful_Array_Class_for_CUDA
	cudaError_t result = cudaMalloc((void**)&simulationsGPU, nSimWidth*nSimHeight*nSimDepth * sizeof(double));
	if (result != cudaSuccess)
	{
		throw std::runtime_error("failed to allocate device memory");
	}

	result = cudaMalloc((void**)&simulationsGPUFloat, nSimWidth*nSimHeight*nSimDepth * sizeof(float));
	if (result != cudaSuccess)
	{
		throw std::runtime_error("failed to allocate device memory");
	}

	// construct a trivial random generator engine from a time-based seed:
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

	//init random generator CPU
	generator = new std::default_random_engine(seed);
	distribution = new std::normal_distribution<double>(0.0, 1.0);
}



NormalSim::~NormalSim()
{
	delete[] simulations;
	delete distribution;
}

void NormalSim::simulate() {
	int idx = 0;
	//cfr matlab
	//double test[8] = { 0.5201, -0.0200, -0.0348, 0.0105 , - 0.7982, 1.0187, -0.1332, -0.350};
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < depth; k++)
			{
				//test matlab
				//simulations[idx] = test[idx];
				simulations[idx] = (*distribution)(*generator);
				simulationsFloat[idx] = simulations[idx];
				//cout << simulations[idx]  << "\n";
				idx++;
			}
		}
	}
	//copy random number to device
	cudaError_t result = cudaMemcpy(simulationsGPU, simulations, width*height*depth * sizeof(double), cudaMemcpyHostToDevice);
	if (result != cudaSuccess)
	{
		throw std::runtime_error("failed to copy to device memory");
	}

	//copy random number to device
	result = cudaMemcpy(simulationsGPUFloat, simulationsFloat, width*height*depth * sizeof(float), cudaMemcpyHostToDevice);
	if (result != cudaSuccess)
	{
		throw std::runtime_error("failed to copy to device memory");
	}

	//just to test if data are correct
	/*double *test = new double[width*height*depth];
	cudaMemcpy(test, simulationsGPU, width*height*depth * sizeof(double), cudaMemcpyDeviceToHost);
	idx = 0;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < depth; k++)
			{
				if (test[idx]!= simulations[idx])
				{
					throw std::runtime_error("COpy is not identical");
				}
				idx++;
			}
		}
	}
	delete[] test;*/
}