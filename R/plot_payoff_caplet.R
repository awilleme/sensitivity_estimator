library("RColorBrewer")

setwd('D:\\Documents\\Actuariat\\tfe\\implementation\\c++\\Sensitivity_estimator\\Results\\')

#cfr. http://www.sthda.com/english/wiki/colors-in-r
colors=brewer.pal(n = 9, name = "Set1")

setEPS()
postscript("payoff_caplet_30_March_2019_BlackVsMeanMC.eps")

x=seq(0, 0.05, by = 0.0001)
K=0.03
y=(unlist(lapply(x, function(x) max(x-K,0))))
plot(x =x, y = y, type="l",lwd=3,
     xlab="Li(Ti) ",ylab="Payoff of caplet i", pch = 1, col=colors[1])
     

#legend(0, max(table[1,]), legend=c("Black prices","Monte-Carlo prices"), box.lty=0,
#       col=colors, lty=1, cex=1, lwd=2, text.font=4)

grid(nx = NULL, ny = NULL, col = "lightgray", lty = "dotted")

dev.off()


