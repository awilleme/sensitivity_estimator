library("RColorBrewer")
setwd('D:\\Documents\\Actuariat\\tfe\\implementation\\c++\\Sensitivity_estimator\\Results\\deltas\\25_March_2019_onlyLast')
Title=paste0("Deltas of caplet on rate i=10 for deltaT=",deltaTprint," and K=",Kprint)
onlyLastString="_onlyLast"

method <- "BlackFormula"
deltaT <- "0o5"
deltaTprint <- "0.05"
K <- "0o03"
Kprint <- "0.03"
nPaths <- "10000"
nRates <- 10

#cfr. http://www.sthda.com/english/wiki/colors-in-r
colors=brewer.pal(n = 9, name = "Set1")

filename=paste0("Deltas_",method,"_deltaT",deltaT,"_K",K,"_nPaths",nPaths,"_nRates",nRates,onlyLastString,".csv")
con <- file(filename, "r")
line <- readLines(con, n = 1)
line <- readLines(con, n = 1)
line <- readLines(con, n = 1)
print(line)
deltas <- as.double(unlist(strsplit(line, split=";"))[1:nRates+1])
ylim <-max(deltas)
setEPS()
postscript(paste("lastDeltas.eps"))
plot(x = 1:nRates, y = deltas, type="o", ylim=c(0,ylim),col=colors[1],lwd=2,
     xlab="Rate j",ylab="Delta", 
     main = Title, pch = 1)
grid(nx = NULL, ny = NULL, col = "lightgray", lty = "dotted")

dev.off()
