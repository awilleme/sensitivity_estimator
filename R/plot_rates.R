library("RColorBrewer")
setwd('D:\\Documents\\Actuariat\\tfe\\implementation\\c++\\Sensitivity_estimator\\Results\\rates\\27_March_2019')

deltaT <- 0.5
deltaTprint <- "0o5"
nPaths <- 1000
nRates <- 10
iRates <- 6
ylimmin <- 0
ylimmax <- 0.12

plottedPaths=500
maxPalette=9;
xlim <- nRates*deltaT

Title=paste0("Paths for 6th LIBOR rate for deltaT=",deltaT," and ",plottedPaths, " paths")

#cfr. http://www.sthda.com/english/wiki/colors-in-r
colors=brewer.pal(n = maxPalette, name = "Set1")

filename=paste0("Rates_deltaT",deltaTprint,"_nPaths",nPaths,"_nRates",nRates,".csv")

table <- read.csv(filename, sep=";", dec=",", stringsAsFactors = FALSE, header=FALSE)

setEPS()
postscript("pathsLibor_27_March_2019.eps")

for(i in 0:(plottedPaths-1)){
  if(i==0){
    plot(x = (0:(nRates-1)*deltaT), y = table[i+2,(((iRates-1)*nRates+1):(iRates*nRates))], type="o", 
         ylim=c(ylimmin,ylimmax), xlim=c(0,xlim),col=colors[(i%%maxPalette)+1],lwd=2,
         xlab="Time (in years)",ylab="Rate", 
         main = Title)
  }
  else{
    lines(x = (0:(nRates-1)*deltaT), y = table[i+2,(((iRates-1)*nRates+1):(iRates*nRates))], 
          type="o",col=colors[(i%%maxPalette)+1], lwd=2)
  }
}
grid(nx = NULL, ny = NULL, col = "lightgray", lty = "dotted")

dev.off()